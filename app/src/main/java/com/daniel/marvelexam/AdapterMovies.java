package com.daniel.marvelexam;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

public class AdapterMovies extends RecyclerView.Adapter<AdapterMovies.MyViewHolder> implements Serializable {
    private List<Movie> movies;
    private Context context;

    public AdapterMovies(List<Movie> movies, Context context) {
        this.movies = movies;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements Serializable {
        private ImageView imageRow;
        private TextView titleRow;
        private TextView descriptionRow;
        private TextView dateRow;
        private RelativeLayout dataRowRelative;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
        imageRow= itemView.findViewById(R.id.imageRow);
        titleRow= itemView.findViewById(R.id.titleRow);
        descriptionRow= itemView.findViewById(R.id.descriptionRow);
        dateRow= itemView.findViewById(R.id.dateRow);
        dataRowRelative= itemView.findViewById(R.id.dataRowRelative);

        }
    }
    @NonNull
    @Override
    public AdapterMovies.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_data, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMovies.MyViewHolder holder, int position) {
        Picasso.get().load(movies.get(position).getUrlToImage())
                .fit()
                .centerCrop()
                .into(holder.imageRow);
        holder.titleRow.setText(movies.get(position).getTitle());
        holder.descriptionRow.setText(movies.get(position).getDescription().substring(0,50));
        holder.dateRow.setText(movies.get(position).getPublishedAt());

        holder.dataRowRelative.setOnClickListener(view -> {
            Intent intent =new Intent(context, DetailsActivity.class);
            intent.putExtra("movie",movies.get(position));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }


}
