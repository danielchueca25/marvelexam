package com.daniel.marvelexam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DetailsActivity extends AppCompatActivity   {
    private Movie movie;
    private ImageView imageBig;
    private TextView titleDetails,dateDetails,descriptionDetails,contentDetails, author;
    private Button webDetails,authorDetails,contactDetails;
    private RecyclerView recyclerView;
    private static final String VIDEO_SAMPLE = "video";
    private WebView videoView;
    ViewPager viewPager;
    ArrayList<String> images = new ArrayList<>();
    private int mCurrentPosition = 0; //version 2
    private static final String PLAYBACK_TIME = "play_time"; //version 2



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        movie = (Movie) getIntent().getSerializableExtra("movie");
        dateDetails = findViewById(R.id.dateDetails);
        author= findViewById(R.id.author);
        titleDetails = findViewById(R.id.titleDetails);
        descriptionDetails = findViewById(R.id.descriptionDetails);
        contentDetails = findViewById(R.id.contentDetails);
        webDetails = findViewById(R.id.webDetails);
        authorDetails = findViewById(R.id.authorDetail);
        contactDetails = findViewById(R.id.contactDetails);
        imageBig = findViewById(R.id.imageGrande);
        videoView =findViewById(R.id.webView);
        recyclerView = findViewById(R.id.comments);
        viewPager = findViewById(R.id.viewPager);
        loadData();


    }

    private void loadData() {
        Picasso.get().load(movie.getUrlToImage())
                .fit()
                .centerCrop()
                .into(imageBig);
        author.setText(movie.getSource().getName());
        titleDetails.setText(movie.getTitle());
        dateDetails.setText(movie.getPublishedAt());
        descriptionDetails.setText(movie.getDescription());
        contentDetails.setText(movie.getContent());
        webDetails.setOnClickListener(view -> {
            Uri uri = Uri.parse(movie.getUrl());
            Intent intent = new Intent(Intent.ACTION_VIEW,uri);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });
        authorDetails.setOnClickListener(view -> {
            Uri uri = Uri.parse(movie.getAuthorlink());
            Intent intent = new Intent(Intent.ACTION_VIEW,uri);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });
        String[] TO = {movie.getContact()};
        String[] CC = {"address1@gmail.com"};

        contactDetails.setOnClickListener(view -> {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
            emailIntent.putExtra(Intent.EXTRA_CC, CC);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject of the mail");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "This is the text to send with the mail");
            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {

            }

        });

        videoView.getSettings().setJavaScriptEnabled(true);
        videoView.setWebChromeClient(new WebChromeClient());
        videoView.loadUrl(movie.getUrlvideo());

        images.add(movie.getMedia().getUrlToImage1());
        images.add(movie.getMedia().getUrlToImage2());
        images.add(movie.getMedia().getUrlToImage3());
        ImageAdapter imageAdapter = new ImageAdapter(getApplicationContext(),images);
        viewPager.setAdapter(imageAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        AdapterComment adapterComment = new AdapterComment(movie.getComments(), getApplicationContext());
        recyclerView.setAdapter(adapterComment);

    }



}