package com.daniel.marvelexam;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

public class AdapterComment extends RecyclerView.Adapter<AdapterComment.MyViewHolder>  {
    private List<String> comments;
    private Context context;

    public AdapterComment(List<String> comments, Context context) {
        this.comments = comments;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements Serializable {

        private TextView titleRow;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            titleRow= itemView.findViewById(R.id.textView);


        }
    }
    @NonNull
    @Override
    public AdapterComment.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.comments_data, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.titleRow.setText(comments.get(position));
    }



    @Override
    public int getItemCount() {
        return comments.size();
    }


}