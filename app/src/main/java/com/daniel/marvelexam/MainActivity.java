package com.daniel.marvelexam;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final String JSON_URL = "https://run.mocky.io/v3/39fcc41e-9d03-486c-8fb2-235e3e831a1b";
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
        searchMovie();

    }
    private void searchMovie() {

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_URL,
                null,
                (Response.Listener<JSONObject>) response -> {
                    List<Movie> results = new ArrayList<>();

                        try {
                            JSONArray jsonArray = response.getJSONArray("articles");

                            results = Arrays.asList(
                                    new GsonBuilder().create().fromJson(jsonArray.toString(),
                                            Movie[].class));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    System.out.println("aaaaaaaaaaaaaaaaaa"+ results.get(0).getMedia().getUrlToImage1());
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

                        AdapterMovies adapter = new AdapterMovies(results, getApplicationContext());
                        recyclerView.setAdapter(adapter);
                },
                error -> Log.d("tag", "onErrorResponse: " + error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }
}